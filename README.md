# Mandelbrot Visualization in Processing

The mandelbrot set is the set of all complex numbers c that stay bounded under the iteration of z^2 + c, starting at z=0. 
Here is an image of such visualzation, zoomed in to a specific region.

![image](https://files.craftsteamg.com/images/2021/11/07/java_O9fIZ2bcZu.png)