final int HEIGHT = 800;
final int WIDTH = 800;
final float SAT = map(.8, 0, 1, 0, 255);
final float VAL = map(1, 0, 1, 0, 255);

float zR = -1;
float zC = 0;
int zoom = 1;
int ITERATIONS = 60;

boolean shouldRender = true;


void settings() {
  size(WIDTH, HEIGHT, P3D);
}

void setup() {
  colorMode(HSB, 60);
  //translate(HEIGHT/2, WIDTH/2);
  loadPixels();
}

void draw() {
  if (shouldRender) {
    for (int i = 0; i < WIDTH; i++) {
      for (int j = 0; j < HEIGHT; j++) {
        
        
        double real = zR + (((double)i / WIDTH) - 0.5) * (1 / (float) zoom) * 4;
        double complex = zC + (((double)j / HEIGHT) - 0.5) * (1 / (float) zoom) * 4;
        
        //System.out.println(real);
        set(i, j, getPixelColor(real, complex));
        //System.out.println(hue);
      }
    }
    shouldRender = false;
  }
  
  textSize((float) WIDTH / 10);
  text(String.format("Zoom: %3dx", zoom), 0, HEIGHT - 5);
  text(String.format("Iterations: %d", ITERATIONS), 0, HEIGHT - (WIDTH / 10) - 5);
}

void keyPressed() {
   switch(key) {
      case '+':
        zoom = 2 * zoom;
        break;
      case '-':
        zoom = max(1, zoom / 2);
        break;
        
      case '8':
        zC -= (1 / (float) zoom) * .1;
        break;
      case '2':
        zC += (1 / (float) zoom) * .1;
        break;
      case '4':
        zR -= (1 / (float) zoom) * .1;
        break;
      case '6':
        zR += (1 / (float) zoom) * .1;
        break;
        
      case '1':
        ITERATIONS = max(0, ITERATIONS - 20);
        break;
      case '3':
        ITERATIONS += 20;
        break;
   }
   shouldRender = true;
   System.out.println(key);
}


color getPixelColor(double constR, double constI) {
  int loops = 0;
  double real = 0;
  double complex = 0;
  while (loops < ITERATIONS) {


    double oldReal = real;
    double oldComplex = complex;
    real = ((oldReal * oldReal) - (oldComplex * oldComplex)) + constR;
    complex = (oldReal * oldComplex * 2) + constI;

    double magSquared = real * real + complex * complex;
    if (magSquared >= 4)
      break;
    loops++;
  }

  //double zn = Math.sqrt(real*real + complex*complex);
  //float a = (float) (loops + 1 - Math.log(Math.log(Math.abs(zn)))/Math.log(2));
  //return color(a, SAT, VAL);
  
  return color(map(loops, 0, ITERATIONS - 1, 0, 255));
}
